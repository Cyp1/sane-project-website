Steve... I hope the following jogs some brain cells.

This is the second problem I found with my snapscan 1212u and 1.0.3 sane
running under kernel 2.4.0-test4

Problem with return value in RGBRouter_remaining() on high density scans

it looks like if the circular buffer is bigger than a TXSource buffer
(ie 32k) then things get ugly. This happens on color 300dpi and 600dpi scans




Reference code used in dumps:

static SANE_Int
RGBRouter_remaining (Source * pself)
{
  RGBRouter *ps = (RGBRouter *) pself;
  static char me[] = "RGBRouter_remaining";

	// the following calcs are suspect, especially at 600dpi scans  
  SANE_Int remaining1 =
    (TxSource_remaining (pself) - ps->cb_size + ps->cb_line_size);
  SANE_Int remaining2 =
    (TxSource_remaining (pself) + ps->cb_line_size - ps->pos);
  SANE_Int remaining;


  if (ps->cb_start < 0)
    remaining = remaining1;
  else
    remaining = remaining2;

  DBG (1,
       "%s TxSource_remaining()= %d, pos= %d, cb_size =%d, cb_line_size=%d, cb_start=%d ch_offset[]=%d %d %d\n",
       me, TxSource_remaining (pself), ps->pos, ps->cb_size, ps->cb_line_size,
       ps->cb_start, ps->ch_offset[0], ps->ch_offset[1], ps->ch_offset[2]);


  DBG (1, "%s %d %d ret=%d\n", me, remaining1, remaining2, remaining);
  
  if ( remaining < 0 ) usleep(100000);	// added so I get a chance to ^c to stop loop
  return remaining;
}



test1- works=====================================================


8.5x11 300dpi greyscale scan


[snapscan] inquiry:  g2r=0 b2r=0 Chroma offsets Red:0 Green:0 Blue:0
[snapscan] inquiry: pixels per scan line = 2539
[snapscan] inquiry: bytes per scan line = 2539
[snapscan] inquiry: number of scan lines = 3299
[snapscan] inquiry: effective buffer size = 30468 bytes, 9 lines
[snapscan] inquiry: expected total scan data: 8376161 bytes
[snapscan] create_base_source type = BUF_SRC
[snapscan] Source_init
[snapscan] inquiry:  g2r=0 b2r=0 Chroma offsets Red:0 Green:0 Blue:0
[snapscan] inquiry: pixels per scan line = 2539
[snapscan] inquiry: bytes per scan line = 2539
[snapscan] inquiry: number of scan lines = 3299
[snapscan] inquiry: effective buffer size = 30468 bytes, 9 lines
[snapscan] inquiry: expected total scan data: 8376161 bytes

[snapscan] create_base_source type = FD_SRC
[snapscan] create_base_source type = SCSI_SRC
[snapscan] Source_init
[snapscan] FDSource_init
[snapscan] NOT doing an inquiry to get scanner parameters
[snapscan] FDSource_remaining 8376161
[snapscan] Source_init
[snapscan] SCSISource_init
[snapscan] SCSISource_remaining 8376161
[snapscan] SCSISource_remaining 8376161
[snapscan] SCSISource_remaining 8376161
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 8372065
[snapscan] child READ 4096 BYTES remaining=8372065
[snapscan] child WROTE 4096 BYTES
[snapscan] SCSISource_remaining 8372065
[snapscan] SCSISource_remaining 8372065
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096


this looks good, the remaining for fd and scsi aggree on # bytes

test2- works============================================================

the following is the same thing with color scanning at 300 dpi
looks funny, but the first part is doing timing with a 30k
BUF_SRC buffer


[snapscan] inquiry:  g2r=130 b2r=132 Chroma offsets Red:4 Green:2 Blue:0
[snapscan] inquiry: pixels per scan line = 2539
[snapscan] inquiry: bytes per scan line = 7617
[snapscan] inquiry: number of scan lines = 3303
[snapscan] inquiry: effective buffer size = 30468 bytes, 9 lines
[snapscan] inquiry: expected total scan data: 25158951 bytes
[snapscan] create_base_source type = BUF_SRC
[snapscan] Source_init
[snapscan] create_RGBRouter
[snapscan] Source_init
[snapscan] TxSource_init status=0
[snapscan] RGBRouter_init chroma_offset[]=4 2 0 bytesPerLine=7617 lines_in_buffer=5 
[snapscan] RGBRouter_init pos= 7617, cb_size =38085, cb_line_size=7617, cb_start=-1 ch_offset[]=30468 17773 5078
[snapscan] TxSource_remaining =30592    
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 7617, cb_size =38085, cb_line_size=7617, cb_start=-1 ch_offset[]=30468 17773 5078
[snapscan] RGBRouter_remaining 124 30592 ret=124
[snapscan] RGBRouter_get plen=7617, remaining=7617 pself->remaining=124
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 7617, cb_size =38085, cb_line_size=7617, cb_start=-1 ch_offset[]=30468 17773 5078

the return value following of 124 is highly suspect but doesnt cause a hang

[snapscan] RGBRouter_remaining 124 30592 ret=124
[snapscan] RGBRouter_get before TxSource_get() ndata = 38085 ndata2=38085 ndata3=0
[snapscan] TxSource_get status=0 plen before=38085, plen after=30592
[snapscan] RGBRouter_get before TxSource_get() ndata = 38085 ndata2=7493 ndata3=30592
[snapscan] TxSource_get status=5 plen before=7493, plen after=7493
[snapscan] RGBRouter_get OOPS early return 5 7493
[snapscan] inquiry:  g2r=130 b2r=132 Chroma offsets Red:4 Green:2 Blue:0
[snapscan] inquiry: pixels per scan line = 2539
[snapscan] inquiry: bytes per scan line = 7617
[snapscan] inquiry: number of scan lines = 3303
[snapscan] inquiry: effective buffer size = 30468 bytes, 9 lines
[snapscan] inquiry: expected total scan data: 25158951 bytes

[snapscan] create_base_source type = FD_SRC
[snapscan] create_base_source type = SCSI_SRC
[snapscan] Source_init
[snapscan] SCSISource_init
[snapscan] SCSISource_remaining 25158951
[snapscan] SCSISource_remaining 25158951
[snapscan] Source_init
[snapscan] FDSource_init
[snapscan] create_RGBRouter
[snapscan] Source_init
[snapscan] TxSource_init status=0
[snapscan] RGBRouter_init chroma_offset[]=4 2 0 bytesPerLine=7617 lines_in_buffer=5 
[snapscan] RGBRouter_init pos= 7617, cb_size =38085, cb_line_size=7617, cb_start=-1 ch_offset[]=30468 17773 5078
[snapscan] NOT doing an inquiry to get scanner parameters
[snapscan] FDSource_remaining 25158951
[snapscan] TxSource_remaining =25158951
[snapscan] FDSource_remaining 25158951
[snapscan] TxSource_remaining =25158951
[snapscan] FDSource_remaining 25158951
[snapscan] TxSource_remaining =25158951
[snapscan] RGBRouter_remaining TxSource_remaining()= 25158951, pos= 7617, cb_size =38085, cb_line_size=7617, cb_start=-1 ch_offset[]=30468 17773 5078
[snapscan] RGBRouter_remaining 25128483 25158951 ret=25128483
[snapscan] SCSISource_remaining 25158951
etc... scan proceeds ok


test3 never gets to scanning========================================================

ok for the fun part lets try 600 dpi scan
this seems to break during the speed testing

[snapscan] inquiry:  g2r=132 b2r=136 Chroma offsets Red:8 Green:4 Blue:0
[snapscan] inquiry: pixels per scan line = 5078
[snapscan] inquiry: bytes per scan line = 15234
[snapscan] inquiry: number of scan lines = 6607
[snapscan] inquiry: effective buffer size = 30468 bytes, 4 lines
[snapscan] inquiry: expected total scan data: 100651038 bytes
[snapscan] create_base_source type = BUF_SRC
[snapscan] Source_init
[snapscan] create_RGBRouter
[snapscan] Source_init
[snapscan] TxSource_init status=0
[snapscan] RGBRouter_init chroma_offset[]=8 4 0 bytesPerLine=15234 lines_in_buffer=9 
[snapscan] RGBRouter_init pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
BAD -ve return value
**** at this point rgb_remaining has returned a bogus remaining value of -91280
**** code in RGBRouter_remaining for calculating the 2 possible remainders follows:
  SANE_Int remaining1 =
    (TxSource_remaining (pself) - ps->cb_size + ps->cb_line_size);
	= 30592 - 137106 + 15234
	= -91280  ... bad news
  SANE_Int remaining2 =
    (TxSource_remaining (pself) + ps->cb_line_size - ps->pos);
	= 30592 + 15234 - 15234
	= 30592
	
because of the bad return value, RGBRouter_get() does nothing and we get stuck in an infinite loop
as shown below:
[snapscan] RGBRouter_get plen=15234, remaining=15234 pself->remaining=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] RGBRouter_get plen=15234, remaining=15234 pself->remaining=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] RGBRouter_get plen=15234, remaining=15234 pself->remaining=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] RGBRouter_get plen=15234, remaining=15234 pself->remaining=-91280
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] TxSource_remaining =30592
[snapscan] RGBRouter_remaining TxSource_remaining()= 30592, pos= 15234, cb_size =137106, cb_line_size=15234, cb_start=-1 ch_offset[]=121872 66014 10156
[snapscan] RGBRouter_remaining -91280 30592 ret=-91280
[snapscan] TxSource_remaining =30592

and so on forever, scan never starts because we never finish speed test due to -ve ret value

test4 - works ==================================================================
600dpi greyscale
[snapscan] inquiry:  g2r=0 b2r=0 Chroma offsets Red:0 Green:0 Blue:0
[snapscan] inquiry: pixels per scan line = 5078
[snapscan] inquiry: bytes per scan line = 5078
[snapscan] inquiry: number of scan lines = 6599
[snapscan] inquiry: effective buffer size = 30468 bytes, 4 lines
[snapscan] inquiry: expected total scan data: 33509722 bytes
[snapscan] create_base_source type = BUF_SRC
[snapscan] Source_init
[snapscan] inquiry:  g2r=0 b2r=0 Chroma offsets Red:0 Green:0 Blue:0
[snapscan] inquiry: pixels per scan line = 5078
[snapscan] inquiry: bytes per scan line = 5078
[snapscan] inquiry: number of scan lines = 6599
[snapscan] inquiry: effective buffer size = 30468 bytes, 4 lines
[snapscan] inquiry: expected total scan data: 33509722 bytes
[snapscan] create_base_source type = FD_SRC
[snapscan] create_base_source type = SCSI_SRC
[snapscan] Source_init
[snapscan] SCSISource_init
[snapscan] SCSISource_remaining 33509722
[snapscan] SCSISource_remaining 33509722
[snapscan] Source_init
[snapscan] FDSource_init
[snapscan] NOT doing an inquiry to get scanner parameters
[snapscan] FDSource_remaining 33509722
[snapscan] SCSISource_remaining 33509722
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 33505626
[snapscan] child READ 4096 BYTES remaining=33505626
[snapscan] child WROTE 4096 BYTES
[snapscan] SCSISource_remaining 33505626
[snapscan] SCSISource_remaining 33505626
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 33501530
[snapscan] child READ 4096 BYTES remaining=33501530
[snapscan] FDSource_remaining 33509722
[snapscan] FDSource_remaining 33509722
[snapscan] FDSource_remaining 33509722
[snapscan] FDSource_remaining 33505626

following line shows where we got 0 bytes and EAGAIN status
the routine has been patched to loop on EAGAIN instead of break

[snapscan] FDSource_get EAGAIN break requested=32768 remaining=28672
[snapscan] child WROTE 4096 BYTES
[snapscan] SCSISource_remaining 33501530
[snapscan] SCSISource_remaining 33501530
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 33497434
[snapscan] child READ 4096 BYTES remaining=33497434
[snapscan] FDSource_remaining 33505626
[snapscan] FDSource_remaining 33501530
[snapscan] FDSource_get EAGAIN break requested=32768 remaining=24576
[snapscan] child WROTE 4096 BYTES
[snapscan] SCSISource_remaining 33497434
[snapscan] SCSISource_remaining 33497434
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 33493338
[snapscan] child READ 4096 BYTES remaining=33493338
[snapscan] FDSource_remaining 33501530
[snapscan] FDSource_remaining 33497434
[snapscan] FDSource_get EAGAIN break requested=32768 remaining=20480
[snapscan] child WROTE 4096 BYTES
[snapscan] SCSISource_remaining 33493338
[snapscan] SCSISource_remaining 33493338
[snapscan] SCSISource_get status=0, plen_in=4096, plen_out=4096
[snapscan] SCSISource_remaining 33489242
[snapscan] child READ 4096 BYTES remaining=33489242
[snapscan] FDSource_remaining 33497434

etc to scan completion

