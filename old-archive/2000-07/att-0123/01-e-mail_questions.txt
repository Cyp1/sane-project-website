[root@Kitty /root]# lsmod
Module                  Size  Used by
parport_probe           3816   0  (autoclean)
parport_pc              7852   1  (autoclean)
lp                      5008   0  (autoclean)
parport                 8284   1  (autoclean) [parport_probe parport_pc lp]
ppp                    21260   0  (autoclean)
slhc                    4572   0  (autoclean) [ppp]
vfat                   11164   0  (autoclean) (unused)
fat                    32864   0  (autoclean) [vfat]
sg                     16192   0  (autoclean) (unused)
usb-uhci               26992   0  (unused)
autofs                  9540   1  (autoclean)
irda                   84353   1
rtl8139                11812   1  (autoclean)
emu10k1                50948   0
soundcore               3748   4  [emu10k1]
keybdev                 1832   0  (unused)
usbkbd                  2788   0  (unused)
input                   3104   0  [keybdev usbkbd]
usb-storage            12324   0  (unused)
usbcore                48648   0  [usb-uhci usbkbd usb-storage]
supermount             15112   2  (autoclean)

 [root@Kitty /root]# modprobe aic7xxx
Note: /etc/conf.modules is more recent than /lib/modules/2.2.16-9mdk/modules.dep 


Contents of /etc/conf.modules

alias sound emu10k1
post-install usb-storage modprobe usbkbd; modprobe keybdev
alias scsi_hostadapter usb-storage
alias parport_lowlevel parport_pc
pre-install plip modprobe parport_pc ; echo 7 > /proc/parport/0/irq
alias block-major-11 scsi_hostadapter
alias usb-interface usb-uhci
pre-install pcmcia_core /etc/rc.d/init.d/pcmcia start
post-install supermount modprobe scsi_hostadapter
alias eth0 rtl8139
alias scsi_hostadapter1 aic7xxx
alias scsi_hostadapter2 aic7xxx          
